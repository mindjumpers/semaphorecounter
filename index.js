/*eslint-env node */

// system modules
var events = require( 'events' );

//@this semaphoreCounter
function semaphoreEnter( key ) {
	var _key = key || '';

	if( this._semaphores.hasOwnProperty( _key ) ) {
		this._semaphores[ _key ] += 1;
	} else {
		this._semaphores[ _key ] = 1;
	}
	this.emit( 'entered', _key );
}

function _semaphoreExitCheck( ){
	var key;
	for( key in this._semaphores ) {
		if( this._semaphores[ key ] ) {
			return;
		}
	}

	this.emit( 'end' );
}

function semaphoreLeave( key ) {
	var _key = key || '';

	if( this._semaphores.hasOwnProperty( _key ) ) {
		this._semaphores[ _key ] -= 1;
		this.emit( 'left', _key );
		_semaphoreExitCheck.call( this );
	}
}

function semaphoreStatus( key ) {
	var _key;

	if( typeof key === 'undefined' ) {
		return this._semaphores;
	}

	_key = key || '';

	return this._semaphores.hasOwnProperty( _key )
		? this._semaphores[ _key ]
		: null;
}

function buildBound( context, method ) {
	return function boundMethod( key ) {
		var _key = key || '';
		return function doBound() {
			context[ method ]( _key );
		};
	};
}

function SemaphoreCounter( ) {
	events.EventEmitter.call( this );
	this._semaphores = {};
	this.bound = {
		enter: buildBound( this, 'enter' ),
		leave: buildBound( this, 'leave' )
	};
}

// make SemaphoreCounter event emitter
SemaphoreCounter.super_ = events.EventEmitter;
SemaphoreCounter.prototype = Object.create( events.EventEmitter.prototype, {
	constructor: {
		value: SemaphoreCounter,
		enumerable: false
	}
} );

SemaphoreCounter.prototype.enter = semaphoreEnter;
SemaphoreCounter.prototype.leave = semaphoreLeave;
SemaphoreCounter.prototype.status = semaphoreStatus;

module.exports = SemaphoreCounter;
