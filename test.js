var SemaphoreCounter = require('./index.js');

function whenDoneA(){
	console.log('rejoice! we are done with A!');
}
function whenDoneB(){
	console.log('rejoice! we are done with B!');
}
function whenDoneC(){
	console.log('rejoice! we are done with C!');
}

var semA = new SemaphoreCounter( );
var semB = new SemaphoreCounter( );
var semC = new SemaphoreCounter( );

semA.on( 'end', whenDoneA );
semB.on( 'end', whenDoneB );
semC.on( 'end', whenDoneC );

semA.enter('foo');
semA.enter('foo');
setTimeout(function(){
	semA.leave('foo');
}, 10 );
setTimeout(function(){
	semA.leave('foo');
}, 10 );

// only on 'default' semaphore
semB.enter();
setTimeout(function(){
	semB.leave();
}, 10 );

semC.enter('foo');
semC.enter('foo');
semC.enter('bar');
setTimeout(function(){
	semC.leave('foo');
}, 10 );
setTimeout(function(){
	semC.leave('foo');
}, 10 );

console.log('status empty', semC.status( '' ) );
console.log('status foo', semC.status( 'foo' ) );

setTimeout(function(){
	semC.leave('bar');
}, 10 );
