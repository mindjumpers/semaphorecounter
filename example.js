
	var SemaphoreCounter = require('./index.js');
	var sem = new SemaphoreCounter( );

	sem.on( 'end', function(){
		console.log('done!');
	} );

	sem.enter( 'bar' );
	sem.enter( 'foo' );
	sem.enter( 'bar' );

	setTimeout( function(){
		console.log( sem.status() ); // -> { bar: 1, foo: 1 }
		sem.leave( 'foo' );
		console.log( sem.status() ); // -> { bar: 1 }
	}, 2000 );

	setTimeout( sem.bound.leave( 'bar' ), 100 );

	setTimeout( function(){
		sem.leave( 'bar' );
	}, 3000 );

	console.log( sem.status() ); // -> { bar: 2, foo: 1 }
